﻿using Summer.Batch.Extra;
using Summer.Batch.Infrastructure.Item.File.Mapping;
using Summer.Batch.Infrastructure.Item.File.Transform;

namespace MyFirstBatchApplication.Business.Mappers
{
    public class FlatFileRecordMapper2 : IFieldSetMapper<FlatFileRecord2>
    {
        private IDateParser _dateParser = new DateParser();

        /// <summary>
        /// Parser for date columns.
        /// </summary>
        private IDateParser DateParser { set { _dateParser = value; } }

        /// <summary>
        /// Maps a <see cref="IFieldSet"/> to a <see cref="FlatFileRecord2" />.
        /// <param name="fieldSet">the field set to map</param>
        /// <returns>the corresponding item</returns>
        /// </summary>
        public FlatFileRecord2 MapFieldSet(IFieldSet fieldSet)
        {
            // Create a new instance of the current mapped object
            return new FlatFileRecord2
            {
                Code = fieldSet.ReadInt(0),
                Name = fieldSet.ReadString(1), // ReadString trims the read string, use ReadRawString to keep trailing spaces
                Description = fieldSet.ReadString(2),
                Date = _dateParser.Decode(fieldSet.ReadString(3))
            };
        }
    }
    
}